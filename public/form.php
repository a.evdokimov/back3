<?php
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
  if (!empty($_GET['save'])) {
    // Если есть параметр save, то выводим сообщение пользователю.
    print('Спасибо, результаты сохранены.');
  }
  // Включаем содержимое файла form.php.
  //include('form.php');
  // Завершаем работу скрипта.
  exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

// Проверяем ошибки.

$errors = FALSE;
if (empty($_POST['name'])) {
  print('введите имя<br/>');
  $errors = TRUE;
}

if (empty($_POST['email'])) {
  print('Введите email.<br/>');
  $errors = TRUE;
}

if (empty($_POST['sex'])) {
  print('Выберете пол.<br/>');
  $errors = TRUE;
}

if (empty($_POST['year'])) {
  print('Выберете год рождения.<br/>');
  $errors = TRUE;
}

if (empty($_POST['limbs'])) {
  print('Выберете кол-во конечностей.<br/>');
  $errors = TRUE;
}

if (empty($_POST['superpowers'])) {
  print('Выберете суперспособность.<br/>');
  $errors = TRUE;
}

if ($errors) {
  print('Произошла ошибочка :/<br>');
  exit();
}

// Сохранение в базу данных.

/*$user = 'u20937';
$pass = '6902593';
$db = new PDO('mysql:host=localhost;dbname=u20937', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
$name = $_POST['name'];
$email = $_POST['email'];
$year_of_birth = $_POST['year'];
$gender = $_POST['sex'];
$num_of_limbs = $_POST['limbs'];
$sp = $_POST['superpowers'];
$biography = $_POST['biography'];*/


// Подготовленный запрос. Не именованные метки.
try {
  $conn = new PDO("mysql:host=localhost;dbname=u20937", 'u20937', '6902593', array(PDO::ATTR_PERSISTENT => true));

    $user = $conn->prepare("INSERT INTO users SET name = ?, email = ?, year = ?, sex = ?, limbs = ?, biography = ?");
    $user -> execute([$_POST['name'], $_POST['email'], $_POST['year'], $_POST['sex'], $_POST['limbs'], $_POST['biography']]);
    $id_user = $conn->lastInsertId();

    $abilitys = $conn->prepare("INSERT INTO abilitys SET id_user = ?");
    $abilitys -> execute([$id_user]);
    $id_abil = $conn->lastInsertId();

    $abil = implode(',',$_POST['superpowers']);

    $ability = $conn->prepare("INSERT INTO ability SET abilitysId = ?, names_ability = ?");
    $ability -> execute([$id_abil, $abil]);

}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}

//  stmt - это "дескриптор состояния".
 
//  Именованные метки.
//$stmt = $db->prepare("INSERT INTO test (label,color) VALUES (:label,:color)");
//$stmt -> execute(array('label'=>'perfect', 'color'=>'green'));
 
//Еще вариант
/*$stmt = $db->prepare("INSERT INTO users (firstname, lastname, email) VALUES (:firstname, :lastname, :email)");
$stmt->bindParam(':firstname', $firstname);
$stmt->bindParam(':lastname', $lastname);
$stmt->bindParam(':email', $email);
$firstname = "John";
$lastname = "Smith";
$email = "john@test.com";
$stmt->execute();
*/

// Делаем перенаправление.
// Если запись не сохраняется, но ошибок не видно, то можно закомментировать эту строку чтобы увидеть ошибку.
// Если ошибок при этом не видно, то необходимо настроить параметр display_errors для PHP.
header('Location: ?save=1');
